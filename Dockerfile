FROM debian:stretch-slim

# Declare and default our image version to 'invalid' for checking later
ARG IMAGE_VERSION=invalid

# Set the image version to the docker metadata
LABEL IMAGE_VERSION=${IMAGE_VERSION}

####################################################
# Do all your normal build process/install in here #
####################################################

# Write the version to a file which is read by entrypoint
RUN echo $IMAGE_VERSION > /IMAGE_VERSION

# Copy our entrypoint
COPY ./entrypoint.sh /docker_entrypoint.sh
RUN chmod +x /docker_entrypoint.sh

ENTRYPOINT ["/docker_entrypoint.sh"]