#!/bin/bash

set -e

IMAGE_VERSION=$(cat /IMAGE_VERSION)
echo "Image Version [$IMAGE_VERSION]"

if [[ $IMAGE_VERSION == "invalid" ]]
then
    echo "The version for this image has not been defined at built time, unable to guarentee results without knowing which version you are running. Exiting"
    exit 1
fi

exec "$@"