# Docker Image Tag Versioning

This repo is to demonstrate the usage of Git tags passed by GitLab CICD `$CI_COMMIT_TAG` variables to the docker build stage with `--build-args` which are then exposed into the image via Dockerfile `LABEL` instructions and written to a file within the container `/IMAGE_VERSION` which the `entrypoint.sh` script reads and ensures has been set, preventing running if not.

There is no code, no applicaiton, no nothing - just a `Dockerfile` with a simple image build using `ARG`s a `.gitlab-ci.yml` file which outlines how the image is built whilst providing the build args and image version variables and an `entrypoint.sh` script which handles the image version at runtime.

## Local Running / Testing

Obviously within a local setup GitLab does not exist, but as the only requirement of GitLab here is to inject a build argument variable to the Docker build stage, it is easily mocked out for our local use:


```bash
# First set a version variable for use
VERSION=1.4.2

# Build with the following
docker build --build-arg IMAGE_VERSION=$VERSION -f Dockerfile -t docker-image-tag-versioning:$VERSION .

# Run with the following
docker run --rm -it docker-image-tag-versioning:$VERSION

# Will output:
# Image Version [1.4.2]
```

### Using pre-built remote images

You can also test with images already built by this repository pipeline by choosing a tag at [https://gitlab.com/danstreeter/docker-image-tag-versioning/container_registry/3135465](https://gitlab.com/danstreeter/docker-image-tag-versioning/container_registry/3135465), for example:

```bash
docker run --rm -it registry.gitlab.com/danstreeter/docker-image-tag-versioning:1.1.1

# Will output
# Image Version [1.1.1]

```
